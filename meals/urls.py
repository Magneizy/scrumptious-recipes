from django.urls import path

from meals.views import (
    MealListView,
    MealDetailView,
    MealDeleteView,
    MealCreateView,
    MealUpdateView,
)

urlpatterns = [
    path("", MealListView.as_view(), name="meals_list"),
    path("<int:pk>/", MealDetailView.as_view(), name="meal_detail"),
    path("new/", MealCreateView.as_view(), name="meal_new"),
    path("<int:pk>/edit/", MealUpdateView.as_view(), name="meal_edit"),
    path("<int:pk>/delete/", MealDeleteView.as_view(), name="meal_delete"),
]
