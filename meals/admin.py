from django.contrib import admin
from meals.models import Meal
# Register your models here.

class MealAdmin(admin.ModelAdmin):
    pass


admin.site.register(Meal, MealAdmin)