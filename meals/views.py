from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from meals.models import Meal

# Create your views here.


class MealListView(LoginRequiredMixin, ListView):
    model = Meal
    template_name = "meals/list.html"
    paginate_by = 2

    def get_queryset(self):
        return Meal.objects.filter(owner=self.request.user)


class MealDetailView(LoginRequiredMixin, DetailView):
    model = Meal
    template_name = "meals/detail.html"

    def get_queryset(self):
        return Meal.objects.filter(owner=self.request.user)


class MealCreateView(LoginRequiredMixin, CreateView):
    model = Meal
    template_name = "meals/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("my_model_detail", pk=plan.id)


class MealUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal
    template_name = "meals/edit.html"
    fields = ["name", "date"]

    def get_queryset(self):
        return Meal.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_detail", args=[self.object.id])


class MealDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal
    template_name = "meals/delete.html"
    success_url = reverse_lazy("meals_list")

    def get_queryset(self):
        return Meal.objects.filter(owner=self.request.user)
