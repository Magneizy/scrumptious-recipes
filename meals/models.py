from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class Meal(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meals",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")

    def get_queryset(self):
        return Meal.objects.filter(owner=self.request.user)

    def __str__(self):
        return self.name